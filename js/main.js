"use strict"

/* 
    1. Опишіть, як можна створити новий HTML тег на сторінці.

         - за допомогою конструктора Document та методу createElement (document.createElement('')),
        де у '' потрібно вказати назву нового тегу;
         - за допомогою додавання рядка у innerHTML та insertAdjacentHTML;


    2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

        Визначає місце розташування відностно вказаного елемента.
        До відкриваючого чи після закриваючого тега ('beforebegin', 'afterend').
        На початку чи в кінці тегів самого елемента(до чи після дочірніх вузлів) ('afterbegin','beforeend').

    3. Як можна видалити елемент зі сторінки?

        За допомогою конструктора Element.remove().

*/

const array = ["Kharkiv", "Kiyv", ["Borispol", "Irpin",["Bucha","Vorzel"]], "Odessa", "Lviv","Dnipro"];


const getMainList = (arr, tag = document.body) => {
    let ulElement = document.createElement('ul');
    
    arr.forEach(e => {
      Array.isArray(e) ? getMainList(e,ulElement) : ulElement.innerHTML += `<li>${e}</li>`;
    });
    
    tag.append(ulElement);
};

getMainList(array);

setTimeout(() => {
  toCleanPage(3);

});

function toCleanPage(sec) {
    const showTimer = document.querySelector('span');
  
    let timer = sec;

    setInterval(() => {
      timer--;
      showTimer.style.backgroundColor = 'red';
      showTimer.style.color = 'white';
      showTimer.innerText = `До видалення списку ${timer} секунд...`;
  
      if (timer === 0) {
        clearInterval();
        document.body.innerHTML = 'Список видалений!';
      }
    }, 1000);
  }
  